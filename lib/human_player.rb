class HumanPlayer
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_move
    puts 'Where would you like to move? Answer as such: Row, Column'
    gets.chomp.split(', ').map(&:to_i)
  end

  def display(board)
    row0 = '| '
    3.times do |idx|
      row0 << (board.empty?([0, idx]) ? '  |' : board.grid[0][idx].to_s)
    end
    row1 = '| '
    3.times do |idx|
      row1 << (board.empty?([1, idx]) ? '  |' : board.grid[1][idx].to_s)
    end
    row2 = '| '
    3.times do |idx|
      row2 << (board.empty?([2, idx]) ? '  |' : board.grid[2][idx].to_s)
    end

    puts row0
    puts row1
    puts row2
  end
end
