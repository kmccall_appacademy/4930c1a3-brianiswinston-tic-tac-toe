class Board
  attr_reader :angles, :answer
  attr_accessor :grid

  def initialize(grid = Array.new(3) { Array.new(3, nil) })
    @grid = grid
  end

  def place_mark(arr, sym)
    grid[arr[0]][arr[1]] = sym
  end

  def place_marks(arr, sym)
    if arr[0].is_a?(Integer)
      place_mark(arr, sym)
    else
      arr.each { |pos1, pos2| grid[pos1][pos2] = sym }
    end
  end

  def empty?(arr)
    grid[arr[0]][arr[1]].nil?
  end

  def angled
    @angles = [grid[0][0], grid[1][1], grid[2][2]],
              [grid[0][2], grid[1][1], grid[2][0]],
              [grid[0][0], grid[1][0], grid[2][0]],
              [grid[0][1], grid[1][1], grid[2][1]],
              [grid[0][2], grid[1][2], grid[2][2]],
              grid[0],
              grid[1],
              grid[2]
  end

  def winner
    angled
    @answer = nil
    @angles.each do |rows|
      @answer = :X if rows == [:X, :X, :X]
      @answer = :O if rows == [:O, :O, :O]
    end
    @answer
  end

  def tied?
    if @answer != :X || @answer != :O
      return true if @angles.none? { |rows| rows.include?(nil) }
    end
    false
  end

  def over?
    angled
    return true if tied? == true
    winner != nil
  end
end
