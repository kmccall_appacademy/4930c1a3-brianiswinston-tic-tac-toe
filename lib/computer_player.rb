class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    possible = []
    3.times do |row|
      3.times do |col|
        possible << [row, col] if board.grid[row][col].nil?
      end
    end

    possible.each do |move|
      return move if winning_move?(move)
    end

    possible.sample
  end

  def winning_move?(move)
    board.place_mark(move, mark)
    if board.winner == mark
      board.grid[move[0]][move[1]] = nil
      return true
    else
      board.grid[move[0]][move[1]] = nil
      return false
    end
  end
end
